/**
 *
 * @author gabriel
 */
public class TelaInicial extends javax.swing.JFrame {

    /**
     * Creates new form TelaInicial
     */
    public TelaInicial() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ButaoPotenciaF = new javax.swing.JButton();
        ButaoDistorcao = new javax.swing.JButton();
        BotaopotenciaH = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Calculadora");

        ButaoPotenciaF.setText("Fluxo de potência fundamental");
        ButaoPotenciaF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButaoPotenciaFActionPerformed(evt);
            }
        });

        ButaoDistorcao.setText("Distorção harmônica");
        ButaoDistorcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButaoDistorcaoActionPerformed(evt);
            }
        });

        BotaopotenciaH.setText("Fluxo de potência harmônica");
        BotaopotenciaH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotaopotenciaHActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(78, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(ButaoPotenciaF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BotaopotenciaH, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ButaoDistorcao, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(76, 76, 76))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addComponent(ButaoPotenciaF)
                .addGap(26, 26, 26)
                .addComponent(ButaoDistorcao)
                .addGap(26, 26, 26)
                .addComponent(BotaopotenciaH)
                .addContainerGap(57, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ButaoPotenciaFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButaoPotenciaFActionPerformed
        FluxoFundamental obj = new FluxoFundamental();
        obj.setVisible(true);
    }//GEN-LAST:event_ButaoPotenciaFActionPerformed

    private void ButaoDistorcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButaoDistorcaoActionPerformed
        Distorcao obj = new Distorcao();
        obj.setVisible(true);
        
    }//GEN-LAST:event_ButaoDistorcaoActionPerformed

    private void BotaopotenciaHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotaopotenciaHActionPerformed
        FluxoPotenciaH obj = new FluxoPotenciaH();
        obj.setVisible(true);
    }//GEN-LAST:event_BotaopotenciaHActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaInicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotaopotenciaH;
    private javax.swing.JButton ButaoDistorcao;
    private javax.swing.JButton ButaoPotenciaF;
    // End of variables declaration//GEN-END:variables
}
