import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 *
 * @author gabriel
 */
public class GraficoOnda extends javax.swing.JFrame {
 int x, y;
 float t;
    /**
     * Creates new form GraficoOnda
     */
    public GraficoOnda() {
      //  initComponents();
        setSize(1200,1200);
        setResizable(true);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        x=30;
        y=300;
    }
   public void paint (Graphics g){
        g.drawLine(300, 30, 300, 600);
        g.drawLine(30, 300, 600, 300);
        g.setColor(Color.blue);
        g.fillOval(x, y, 3, 3);
        run();
        g.setColor(Color.red);
        run1();
        repaint();
   }
   
   public void recebe(float recebe){
       t=recebe;
   }
   
   public void run ()
   {
       try {
           Thread.sleep(10);
           int ax,by;
           ax = x-300;
           by= y-300;
           float ta = (float) Math.toRadians(t);
           int bby = (int) ((int) t*Math.cos((2*3.1415*60)*ax+ta));
           by = (int) ((int) 50*Math.cos(0.2*bby/3.14));
           x = ax+300;
           y = 300-by;
           x++;
       }catch(Exception e){
           System.out.println("Error !!");
       }
       
   }
   
   
   public void run1()
   {
    try {
           Thread.sleep(10);
           int cx,dy;
           cx = x-300;
           dy= y-300;
           float ta = (float) Math.toRadians(t);
           //int bby = (int) ((int) t*Math.cos((2*3.1415*60)*ax+ta));
           dy = (int) ((int) 50*Math.sin(0.2*cx/3.14));
           x = cx+300;
           y = 300-dy;
           x++;
       }catch(Exception e){
           System.out.println("Error !!");
       }   
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GraficoOnda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GraficoOnda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GraficoOnda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GraficoOnda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GraficoOnda().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
